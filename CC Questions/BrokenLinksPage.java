package week2.day1HW;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrokenLinksPage {
	
	public static void main(String[] args) throws IOException {
		
		
		System.setProperty("webdriver.chrome.driver", "F:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		List<WebElement> links = driver.findElementsByTagName("a");
		int totallinks = links.size();
		System.out.println("Total links : "+totallinks);
		
		for(int i =0;i<links.size();i++) {
			
			WebElement eachlink = links.get(i);
			String url = eachlink.getAttribute("href");
			verifyValidUrl(url);
			
		}
		
	}
	
	public static void verifyValidUrl(String linkurl) throws IOException  {
		try {
			URL ecurl = new URL(linkurl);
			
				HttpURLConnection httpURLConnect = (HttpURLConnection)ecurl.openConnection();
				httpURLConnect.setConnectTimeout(5000);
				httpURLConnect.connect();
				
				if(httpURLConnect.getResponseCode()==200) {
					
					System.out.println(linkurl+" - "+httpURLConnect.getResponseMessage());
				}
				if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND) {
					
					System.out.println(linkurl+" - "+httpURLConnect.getResponseMessage()+" - " + HttpURLConnection.HTTP_NOT_FOUND);
				}
			 
			
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		}
		
		
	}
	
	

}
