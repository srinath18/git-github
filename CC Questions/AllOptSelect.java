package week2.day1HW;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AllOptSelect {

	public static void main(String[] args) throws InterruptedException{

		System.setProperty("webdriver.chrome.driver", "F:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		Thread.sleep(2000);
		WebElement source = driver.findElementById("userRegistrationForm:countries");
		Select drpopt = new Select(source);
		List<WebElement> alloptions =drpopt.getOptions();
		for(WebElement eacopt : alloptions) {
			
			System.out.println("Country name is : "+eacopt.getText());
			
		}

	}
}