package week2.day1HW;

public class CharOccurPercentage {
	
	public static void main(String[] args) {
		
		String s = "Tiger Runs @ The Speed Of 100 km/hour.";
		int upperct = 0;
		int lowerct = 0;
		int digitct = 0;
		int specialchar =0;
		
		for(int i=0;i<s.length();i++) {
			
			if(Character.isUpperCase(s.charAt(i))) {
				upperct++;
			}
			else if(Character.isLowerCase(s.charAt(i))) {
				lowerct++;
			}
			else if(Character.isDigit(s.charAt(i))) {
				digitct++;
			}
			else  {
				specialchar++;
				
			}
			
		}
		
	
		System.out.println("UpperCase Percentage : "+(upperct*100)/38);
		System.out.println("LowerCase Percentage : "+(lowerct*100)/38);
		System.out.println("DigitCount Percentage : "+(digitct*100)/38);
		System.out.println("Other char Percentage : "+(specialchar*100)/38);
		
		
	}

}
