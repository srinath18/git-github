package week2.day1HW;

public class CharOccurance {
	
	public static void main(String[] args) {
		
		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;
		int e = 0;
		int f = 0;
		int g = 0;
		int h = 0;
		String S = "COGNIZANT";
		
		char[] chary = S.toCharArray();
		
		for(char ecch : chary) {
			
			if(ecch == 'C') {
				a++;
			}
			
			else if(ecch == 'O'){
				b++;
			}
			
			else if(ecch == 'G') {
				c++;
			}
			
			else if(ecch == 'N') {
				d++;
			}
			else if(ecch == 'I') {
				e++;
			}
			else if(ecch == 'Z') {
				f++;
			}
			else if(ecch == 'A') {
				g++;
			}
			else if(ecch == 'T') {
				h++;
			}
		}
		
		System.out.println("C occurance : "+a);
		System.out.println("O occurance : "+b);
		System.out.println("G occurance : "+c);
		System.out.println("N occurance : "+d);
		System.out.println("I occurance : "+e);
		System.out.println("Z occurance : "+f);
		System.out.println("A occurance : "+g);
		System.out.println("T occurance : "+h);
	}

}
