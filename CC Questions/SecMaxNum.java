package week2.day1HW;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SecMaxNum {
	
	public static void main(String[] args) {
		
		int[] intArray = {20,340,21,879,92,21,474,83647,-200};
		ArrayList<Integer> lt = new ArrayList();
		
		Arrays.sort(intArray);
		
		for(int eca:intArray) {
			lt.add(eca);
		}
		
		Collections.reverse(lt);
		
		System.out.println("Second largest number is : "+lt.get(1));
		
	}

}
