package week2.day1HW;

public class StringPalindrome {
	
	public static void main(String[] args) {
		
		String sb = "AMMA";
		String sr ="";
		
		for(int i =sb.length()-1;i>=0;i--) {
		
			sr =sr+sb.charAt(i);
			
		}
		
		if(sr.equals(sb)) {
			
			System.out.println("Palindrome");
		}
		else {
			System.out.println("Not Palindrome");
		}
	}

}
