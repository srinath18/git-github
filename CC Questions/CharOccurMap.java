package week2.day1HW;

import java.util.HashMap;
import java.util.Map;

public class CharOccurMap {
	
	public static void main(String[] args) {
		
		String s = "COGNIZANT";
		int sl = s.length();
		
		Map<Character,Integer> mapv = new HashMap();
		
		for(int i=0;i<sl;i++) {
			
			char chve = s.charAt(i);
			
			if(!mapv.containsKey(chve)) {
				mapv.put(chve, 1);
			}
			else {
				mapv.put(chve, mapv.get(chve)+1);
			}
			
		}
		System.out.println(mapv);
		
	}

}
