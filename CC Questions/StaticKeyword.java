package week2.day1HW;

public class StaticKeyword {

	static String a = "Hello";
	String b ="World";
	
	public void nonstaticmethod() {
		System.out.println(b);
	}
	
	public static void staticmethod() {
		
		StaticKeyword var1 = new StaticKeyword();
		System.out.print("Static Method: "+a);
		var1.nonstaticmethod();
	}

	public void instancemethod(String a) {
		this.a = a;
		System.out.println("Instance Method: "+(a+b));
		
	}
	
	public static void main(String[] args) {

		StaticKeyword.staticmethod();
		StaticKeyword vari = new StaticKeyword();
		vari.instancemethod(a);
	}

	
	

}
