package week2.day1HW;

import java.util.HashSet;
import java.util.Set;

public class ArrayDuplSet {
	
	public static void main(String[] args) {
		
		int[] a = {13,65,15,67,88,65,13,99,67,13,65,87,13};
		Set<Integer> nonduplicate = new HashSet();
		Set<Integer> duplicate = new HashSet();
		
		for(int i=0;i<=a.length-1;i++) {
			if(!nonduplicate.contains(a[i])) {
				nonduplicate.add(a[i]);
			}
			else {
				duplicate.add(a[i]);
			}
		}
		
	System.out.println(duplicate);	
		
		
	}

}
