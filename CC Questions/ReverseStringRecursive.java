package week2.day1HW;

public class ReverseStringRecursive {
	
	public static void main(String[] args) {
		
		/*
		 * Normal approach to reverse String
		 * String s = "SRINATH";
		String output =  "";
		int length = s.length();
		
		for(int i= s.length()-1;i>=0;i--) {
			char ecch = s.charAt(i);
			output = output+ecch;
		}
		System.out.println(output);*/
		
		//Using Recursive function
		
		System.out.println(reverse("HELLO"));;
		
		
	}
	
	public static String reverse(String str) {
		
		if(str.length()<=1) {
				
				return str;
		}
		else
			return reverse(str.substring(1, str.length()))+str.charAt(0);
		
	}

}
