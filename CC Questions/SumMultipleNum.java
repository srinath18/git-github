package week2.day1HW;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SumMultipleNum {

	public static void main(String[] args){
		
		int a = 5;
		int b = 3;
		int c = 0;
		int d = 0;
		int sumvalue = 0;
		
		Set<Integer> al = new HashSet<Integer>();
		
		for(int i=1;i<20;i++) {
			c = 5*i;
			al.add(c);
		} 
		
		for(int j=1;j<34;j++) {
			d = 3*j;
			al.add(d);
		}
		
		for(int ec:al) {
			
			sumvalue = sumvalue+ec;
		}
		System.out.println("Sum of multiples of 3 or 5 less than 100 is : "+sumvalue);
		
	}
}
