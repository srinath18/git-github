package week2.day1HW;

import java.util.ArrayList;
import java.util.List;

public class StringUnique {
	
	public static void main(String[] args) {
		
		String s = "Good Looking";
		String ab = s.toLowerCase();
	
		List<Character> lst = new ArrayList<Character>();
		for(int i=0;i<ab.length();i++) {
			
			char ecch = ab.charAt(i);
			
			if(!lst.contains(ecch)) {
				
				lst.add(ecch);
			}
		}
		
		String unique = lst.toString();
		String uniquecom = unique.replaceAll(",", "");
		String uniquefinal = uniquecom.replaceAll(" ", "");
		System.out.println(uniquefinal);
		
		
	}

}
