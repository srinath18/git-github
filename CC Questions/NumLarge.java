package week2.day1HW;

import java.util.ArrayList;
import java.util.Scanner;

public class NumLarge {

	public static void main(String[] args) {

	System.out.println("Enter number : ");
	Scanner nums = new Scanner(System.in);
	int num = nums.nextInt();
	System.out.println("Enter digit : ");
	Scanner digi = new Scanner(System.in);
	int digit = digi.nextInt();
	num--;
	while(Integer.toString(num).contains(Integer.toString(digit))) {
		
		num--;
	}
	
	System.out.println("Greatest number is : "+num);
	}

}
