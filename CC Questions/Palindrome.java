package week2.day1HW;

public class Palindrome {

	public static void main(String[] args) {

		int num = 25852;
		int f= num;
		int reminder = 0;
		int s = 0;

		while(num > 0) {
			reminder = num%10;

			num = num/10;
			s = s*10+reminder;
		}	
		
		if(f == s) {
		System.out.println(s+" Palindrome number");
		}
		else {
			System.out.println(s+" Not Palindrome number");
		}
	}

}
