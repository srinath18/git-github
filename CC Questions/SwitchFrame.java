package week2.day1HW;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SwitchFrame {
	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "F:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//h5[text()='Frame']").click();
		driver.switchTo().frame(0);
		driver.findElementByXPath("//button[text()='Click Me']").click();
		
		
	}

}
