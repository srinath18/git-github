package week2.day1HW;

public class FirstPrimeNums {
	
	public static void main(String[] args) {
		
		int n = 30;
		
		for(int i=1;i<=n;i++) {
			
			boolean flag = true;
			
			for(int j=2;j<i-1;j++) {
				
				if(i%j == 0) {
					
					flag = false;
					break;
					
				}
				
			}
			
			if(flag == true) {
				
				System.out.println(i);
			}
			
		}
		
		
	}

}
