package week2.day1HW;

public class Audi extends Car{

	Audi(){
		System.out.println("Audi Class");
	}
	
	public static void main(String[] args) {
		Audi obj = new Audi();
	}
	
}

class Vehicle{
	Vehicle(){
		System.out.println("Vehicle Class");
	}
}

class Car extends Vehicle{
	Car(){
		System.out.println("Car Class");
	}
}