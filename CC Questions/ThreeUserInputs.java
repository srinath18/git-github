package week2.day1HW;

import java.util.Scanner;

public class ThreeUserInputs {
	
	public static void main(String[] args) {
		
		Scanner scanv = new Scanner(System.in);
		System.out.println("Enter Integer A Value : ");
		int value1 = scanv.nextInt();
		
		System.out.println("Enter Integer B value :");
		int value2 = scanv.nextInt();
		
		System.out.println("Enter Arithmetic operator : ");
		String strvle = scanv.next();
		
		switch (strvle) {
		
		case "ADD":
			System.out.println(value1+value2);
			break;

		case "SUB":	
			System.out.println(value1-value2);
			break;
			
		case "MUL":
			System.out.println(value1*value2);
			break;
		default:
			System.out.println("Invalid value");
			break;
		}
		
		scanv.close();
	}

}
