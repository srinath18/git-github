package week2.day1HW;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrokenImage {
	
	public static void main(String[] args) throws MalformedURLException  {
		
		System.setProperty("webdriver.chrome.driver", "F:\\GradleProject\\Workspace\\Selenium\\drivers\\chromedriver_win32\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.naukri.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> linklists = driver.findElementsByTagName("a");
		int totallinks = linklists.size();
		System.out.println("Total number of links is : "+totallinks);
		
		for(int i = 0;i<linklists.size();i++) {
			WebElement link = linklists.get(i);
			String urlname = link.getAttribute("href");
			verifyValidUrl(urlname);
			
			
		}
		
		
	}
	
	public static void verifyValidUrl(String urlname)   {
		
		
		try {
			
			URL urlobj = new URL(urlname);
			HttpURLConnection linkconnect;
			linkconnect = (HttpURLConnection)urlobj.openConnection();
			
			linkconnect.setConnectTimeout(5000);
			linkconnect.connect();
			if(linkconnect.getResponseCode() == 200) {
				
				System.out.println(urlname+" --> "+linkconnect.getResponseMessage());
			}
			else if(linkconnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND){
				
				System.out.println(urlname+" --> "+linkconnect.getResponseMessage());
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
	}

}
